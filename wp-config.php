<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'uniduck');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '|D4bai$p%Y]!2PM_`YNB)5(ugt}_(L]W496;!oe(|=7uJnINehI;p]%PLROZ=z$!');
define('SECURE_AUTH_KEY',  'p9y.p[$@C7;JnQ=}G6Th0 yg0U<*i=&,QZdx6_%(F(hu:(ziF@.lc.D x]6?pmPm');
define('LOGGED_IN_KEY',    'Vk=mLq`4u9Zc xenNvq*#MSW@0r%pBa|H4Y%(Y707q2 >-O{k0dD<X5W.v.):#*5');
define('NONCE_KEY',        'S6zq3XyIyd,;kr>sul=)o)3cfjs!B>AQoKB~>I&YZgbF_C. <a_ATh4>bzr|I11J');
define('AUTH_SALT',        'oVU(gMNhRfwq <;gWH_]+>#$@/-AV@{hume9,S#0@h.6eY6Hlw]LI8vz<t;^@YR=');
define('SECURE_AUTH_SALT', '!9VGu@9Wi6s!AwUSIhxq`Y0:Gz.!* O0&{0^,a@Jl,D3PJ#@b%b~;2u) p#CAqAP');
define('LOGGED_IN_SALT',   'k9sbNQOL5:Lxl=[:|C$x_vFvEYr6Qn~9b@qR_yL7]f$=QCIj1kC<9.*:y,!l#&s0');
define('NONCE_SALT',       ']bZ< >dzPr<z,L))RLLgp~~t !wKvcK8t8:bE6RGPRa<Ao2Q-mU,<0b>x=1a!e>P');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wordpress_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
