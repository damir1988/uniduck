<?php get_template_part('template-parts/newsletter-template'); ?>
<footer class="mainFooter">
    <div class="container">
        <div class="row">
            <div class="footerInner clearfix">
                <div class="col-sm-6 col-sm-push-6 text-right">
                    <?php
                        $optionFacebook = get_option('footer_social_text_facebook');
                        $optionTwitter = get_option('footer_social_text_twitter');
                        $optionInstagram = get_option('footer_social_text_instagram');
                    ?>
                    <?php if(count($optionFacebook) > 0 || count($optionTwitter) > 0  || count($optionInstagram) > 0 ) : ?>
                    <ul class="socialList">
                            <?php if(count($optionFacebook) > 0) : ?>
                                <li><?php echo $optionFacebook; ?></li>
                            <?php endif; ?>
                            <?php if(count($optionTwitter) > 0) : ?>
                                <li><?php echo $optionTwitter; ?></li>
                            <?php endif; ?>
                            <?php if(count($optionInstagram) > 0) : ?>
                                <li><?php echo $optionInstagram; ?></li>
                            <?php endif; ?>
                    </ul>
                    <?php endif; ?>
                </div>
                <div class="col-sm-6 col-sm-pull-6">
                    <a href="<?php echo get_home_url(); ?>" class="logo">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/Logo.png" srcset="<?php echo get_template_directory_uri(); ?>/assets/images/Logo@2x.png 2x" alt="Uniduck">
                    </a>
                    <p class="copy"><?php echo get_option('copyright_text'); ?></p>
                    <?php wp_nav_menu( array( 'theme_location' => 'footer-navigation', 'container' => 'nav', 'container_class' => '', 'menu_class' => 'secondMenu clearfix', 'menu_id' => '' ) ); ?>
                </div>
            </div>
        </div>
    </div>
</footer>
<?php wp_footer(); ?>
</body>
</html>