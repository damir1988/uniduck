<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php bloginfo('name') ?></title>


    <?php wp_head(); ?>

    <!--[if lt IE 9]>
    <script src="js/ie/js/html5shiv.min.js"></script>
    <script src="js/ie/js/respond.min.js"></script>
    <noscript></noscript>
    <![endif]-->

</head>
<body>
<header class="mainHeader">
    <div class="container">
        <div class="row">
            <div class="col-md-2 col-sm-6 col-xs-6">
                <a href="<?php echo get_home_url(); ?>" class="logo">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/Logo.png" srcset="<?php echo get_template_directory_uri(); ?>/assets/images/Logo@2x.png 2x" alt="Uniduck" />
                </a>
            </div>
            <div class="col-sm-6 col-xs-6 hidden-md hidden-lg text-right">
                <a href="#" class="mobileMenuButton t_upper">Menu</a>
            </div>
            <div class="col-md-10 clearfix visible-md visible-lg">
                <?php
                    $mainButtonText = get_option('header_button_main_text');
                    $mainButtonLink = get_option('header_button_main_link');
                    $secondButtonText = get_option('header_button_second_text');
                    $secondButtonLink = get_option('header_button_second_link');
                ?>
                <?php if(count($mainButtonText) > 0 || count($secondButtonText) > 0) : ?>
                    <ul class="headerButtons">
                        <?php if(count($mainButtonText) > 0) : ?>
                            <li><a href="<?php echo $mainButtonLink; ?>" class="button buttonPrimary buttonIcon icon-apple t_upper round f_ebold anim02"><?php echo $mainButtonText; ?></a></li>
                        <?php endif; ?>
                        <?php if(count($secondButtonText) > 0) : ?>
                            <li><a href="<?php echo $secondButtonLink; ?>" class="button buttonDefault buttonIcon icon-unicorn round f_bold anim02"><?php echo $secondButtonText; ?></a></li>
                        <?php endif; ?>
                    </ul>
                <?php endif; ?>
                <?php wp_nav_menu( array( 'theme_location' => 'main-navigation', 'container' => 'nav', 'container_class' => 'mainNavigation visible-md visible-lg', 'menu_class' => 'mainMenu', 'menu_id' => 'mainMenu' ) ); ?>
            </div>
        </div>
    </div>
    <nav class="mobileMenuWrapper">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <ul class="mobileMenu"></ul>
                </div>
            </div>
        </div>
    </nav>
</header>