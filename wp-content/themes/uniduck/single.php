<?php get_header(); ?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <article class="mainArticle">
        <div class="articleHeading" style="background-image: url(<?php echo get_the_post_thumbnail_url(get_the_ID(), "news_large"); ?>);">
            <div class="overlay"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-push-2 text-center">
                        <h1 class="title"><?php the_title(); ?></h1>
                        <a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>" class="author f_light"><span class="authorIcon"></span><?php echo get_the_author_meta('first_name') . ' ' . get_the_author_meta('last_name'); ?></a>
                        <a href="<?php echo wp_get_referer(); ?>" class="backButton f_medium">Back to blog</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-push-3">
                    <div class="articleInner">
                        <?php the_content(); ?>
                        <?php get_template_part('template-parts/category-list-template'); ?>
                    </div>
                </div>
            </div>
        </div>
    </article>
    <?php get_template_part('template-parts/related-news-template'); ?>
<?php endwhile; endif; ?>
<?php get_footer(); ?>