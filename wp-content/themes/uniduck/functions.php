<?php

/* enqueue scripts and styles */
add_action('wp_enqueue_scripts', 'uniduck_enqueue_files');
function uniduck_enqueue_files(){

    wp_register_style('bootstrap', get_template_directory_uri() . '/css/bootstrap/css/bootstrap.min.css');
    wp_enqueue_style('bootstrap');

    wp_register_style('style', get_template_directory_uri() . '/style.css');
    wp_enqueue_style('style');

    wp_register_script('bootstrap', get_template_directory_uri() . '/js/bootstrap/js/bootstrap.min.js', array('jquery'), false, true);
    wp_enqueue_script('bootstrap');

    wp_register_script('validate', get_template_directory_uri() . '/js/validate/js/jquery.validate.1.12.0.min.js', array('jquery'), '1.0', true);
    wp_enqueue_script('validate');

    wp_register_script('scripts', get_template_directory_uri() . '/js/functions.js', array('jquery'), '1.0', true);
    wp_enqueue_script('scripts');

    wp_register_script( 'ajax-pagination', get_template_directory_uri() . '/js/ajax-pagination.js', array('jquery'), false, true );
    wp_enqueue_script('ajax-pagination');
    wp_localize_script( 'ajax-pagination', 'ajax_object', array( 'ajax_url' => admin_url( 'admin-ajax.php' )) );

    wp_register_script( 'newsletter', get_template_directory_uri() . '/js/newsletter.js', array('jquery'), false, true );
    wp_enqueue_script('newsletter');
    wp_localize_script( 'newsletter', 'ajax_object', array( 'ajax_url' => admin_url( 'admin-ajax.php' )) );

}

/* add more link to excerpt */
function excerpt_readmore( $output ) {
    global $post;
    return $output . ' <a href="' . get_permalink( $post->ID ) . '">Read More</a>';
}
add_filter( 'get_the_excerpt', 'excerpt_readmore' );

/* limit excerpt length */
function custom_excerpt_length($length){
    return 30;
}
add_filter('excerpt_length', 'custom_excerpt_length');


/* register custom post types and taxonomies */
add_action( 'init', 'register_my_types' );
function register_my_types() {
    register_post_type( 'news',
        array(
            'labels' => array(
                'name' => __( 'News' ),
                'singular_name' => __( 'News' )
            ),
            'public' => true,
            'has_archive' => true,
            'supports' => array('title', 'editor', 'author', 'thumbnail', 'comments'),
        )
    );
    register_taxonomy( 'news_categories', array( 'news' ), array(
            'hierarchical' => true,
            'label' => 'News Categories',
            'has_archive' => true,
            'rewrite' => array(
                'slug' => 'categories',
                'hierarchical' => true
            ),
        )
    );
}

/* add our custom permastructures for custom taxonomy and post */
add_action( 'wp_loaded', 'add_clinic_permastructure' );
function add_clinic_permastructure() {

    add_permastruct( 'news_categories', 'news/%news_categories%', false );

    /* .php extension added because subcategory archive was returning 404 - issue */
    add_permastruct( 'news', 'news/%news_categories%/%news%.php', false );
}

/* make sure that all links on the site, include the related texonomy terms */
add_filter( 'post_type_link', 'uniduck_permalinks', 10, 2 );
function uniduck_permalinks( $permalink, $post ) {
    if ( $post->post_type !== 'news' )
        return $permalink;
    $terms = get_the_terms( $post->ID, 'news_categories' );

    if ( ! $terms )
        return str_replace( '%news_categories%/', '', $permalink );

    $post_terms = array();
    $post_terms_parent = null;
    foreach ( $terms as $term ){
        if($term->parent == 0){
            $post_terms_parent = $term->slug;
        }else{
            $post_terms[] = $term->slug;
        }
    }
    if(count($post_terms_parent) > 0){
        $post_terms_parent .= '/';
    }
    $permLink = $post_terms_parent . implode( ',', $post_terms );
    return str_replace( '%news_categories%', $permLink , $permalink );
}

/* thumbnail upscale */
function uniduck_thumbnail_upscale( $default, $orig_w, $orig_h, $new_w, $new_h, $crop ){
    if ( !$crop ) return null;

    $aspect_ratio = $orig_w / $orig_h;
    $size_ratio = max($new_w / $orig_w, $new_h / $orig_h);

    $crop_w = round($new_w / $size_ratio);
    $crop_h = round($new_h / $size_ratio);

    $s_x = floor( ($orig_w - $crop_w) / 2 );
    $s_y = floor( ($orig_h - $crop_h) / 2 );

    return array( 0, 0, (int) $s_x, (int) $s_y, (int) $new_w, (int) $new_h, (int) $crop_w, (int) $crop_h );
}
add_filter( 'image_resize_dimensions', 'uniduck_thumbnail_upscale', 10, 6 );

add_action('after_setup_theme', 'uniduck_after_theme_setup');
function uniduck_after_theme_setup(){

    add_theme_support('post-thumbnails');

    /* custom image sizes */
    add_image_size( 'news_large_retina', 2882, 1940, true );
    add_image_size( 'news_large', 1441, 970, true );
    add_image_size( 'news_thumb_retina', 1110, 740, true );
    add_image_size( 'news_thumb', 555, 352, true );
    add_image_size( 'news_thumb_second_retina', 720, 578, true );
    add_image_size( 'news_thumb_second', 360, 289, true );
}

function related_post() {

    $post_id = get_the_ID();
    $cat_ids = array();
    $categories = get_the_terms( $post_id, 'news_categories' );

    if(!empty($categories)):
        foreach ($categories as $category):
            array_push($cat_ids, $category->term_id);
        endforeach;
    endif;
    $current_post_type = get_post_type($post_id);

    $query_args = array(

        'post_type'         => $current_post_type,
        'post__not_in' => array($post_id),
        'post_status'        => 'publish',
        'posts_per_page'     => 1,
        'orderby'            => 'rand',
        'ignore_sticky_posts'=> true,
        'tax_query'         => array(
            'taxonomy'      => 'news_categories',
            'terms'         => $cat_ids,
        ),


    );

    $related_cats_post = new WP_Query( $query_args );

    return $related_cats_post;
}

/* add post type news to main query */
function add_my_post_types_to_query( $query ) {
    if ( !is_admin() && (is_home() || is_category() || is_archive()) && $query->is_main_query() )
        $query->set( 'post_type', array( 'news', 'post') );
        $query->set( 'posts_per_page', 7 );

    if(is_single())
        $query->set( 'posts_per_page', 1 );

    if(is_admin())
        $query->set( 'posts_per_page', -1 );

    return $query;
}
add_action( 'pre_get_posts', 'add_my_post_types_to_query' );

/* register navigation menu */
add_action('init', 'uniduck_menus');
function uniduck_menus()
{
    register_nav_menu('main-navigation', "Main navigation");
    register_nav_menu('footer-navigation', "Footer navigation");
}

/* customizations */
add_action('customize_register', 'uniduck_customize');

function uniduck_customize($wp_customize) {

    /* copyright */
    $wp_customize->add_section('copyright_section', array(
        'title'          => 'Copyright Text Footer'
    ));
    $wp_customize->add_setting('copyright_text', array(
        'default'        => '© 2015 Uniduck. All rights reserved.',
        'type'        => 'option',
    ));
    $wp_customize->add_control('copyright_text', array(
        'label'   => 'Copyright text',
        'section' => 'copyright_section',
        'type'    => 'text',
    ));

    /* footer social */
    $wp_customize->add_section('footer_social_section', array(
        'title'          => 'Footer social networks'
    ));
    $wp_customize->add_setting('footer_social_text_facebook', array(
        'default'        => 'Like <a href="#" target="_blank">Uniduck</a> on <span class="socialIcon icon-facebook"></span>',
        'type'        => 'option',
    ));
    $wp_customize->add_control('footer_social_text_facebook', array(
        'label'   => 'Facebook social',
        'section' => 'footer_social_section',
        'type'    => 'text',
    ));
    $wp_customize->add_setting('footer_social_text_twitter', array(
        'default'        => 'Follow <a href="#" target="_blank">@uniduck</a> on <span class="socialIcon icon-twitter"></span>',
        'type'        => 'option',
    ));
    $wp_customize->add_control('footer_social_text_twitter', array(
        'label'   => 'Twitter social',
        'section' => 'footer_social_section',
        'type'    => 'text',
    ));
    $wp_customize->add_setting('footer_social_text_instagram', array(
        'default'        => 'Follow <a href="#" target="_blank">@uniduck</a> on <span class="socialIcon icon-instagram"></span>',
        'type'        => 'option',
    ));
    $wp_customize->add_control('footer_social_text_instagram', array(
        'label'   => 'Instagram social',
        'section' => 'footer_social_section',
        'type'    => 'text',
    ));


    /* copyright */
    $wp_customize->add_section('header_buttons_section', array(
        'title'          => 'Header buttons'
    ));
    $wp_customize->add_setting('header_button_main_text', array(
        'default'        => '© 2015 Uniduck. All rights reserved.',
        'type'        => 'option',
    ));
    $wp_customize->add_control('header_button_main_text', array(
        'label'   => 'Main button text',
        'section' => 'header_buttons_section',
        'type'    => 'text',
    ));
    $wp_customize->add_setting('header_button_main_link', array(
        'default'        => '© 2015 Uniduck. All rights reserved.',
        'type'        => 'option',
    ));
    $wp_customize->add_control('header_button_main_link', array(
        'label'   => 'Main button link',
        'section' => 'header_buttons_section',
        'type'    => 'text',
    ));
    $wp_customize->add_setting('header_button_second_text', array(
        'default'        => '© 2015 Uniduck. All rights reserved.',
        'type'        => 'option',
    ));
    $wp_customize->add_control('header_button_second_text', array(
        'label'   => 'Second button text',
        'section' => 'header_buttons_section',
        'type'    => 'text',
    ));
    $wp_customize->add_setting('header_button_second_link', array(
        'default'        => '© 2015 Uniduck. All rights reserved.',
        'type'        => 'option',
    ));
    $wp_customize->add_control('header_button_second_link', array(
        'label'   => 'Second button link',
        'section' => 'header_buttons_section',
        'type'    => 'text',
    ));

}

/* pagination */
function uniduck_pagination() {
    $pages = getPaginateLinks();
    set_query_var( 'pages', $pages );
    get_template_part('template-parts/pagination-template');

}

function getPaginateLinks(){
    global $wp_query;

    if ( $wp_query->max_num_pages <= 1 ) return;

    $big = 999999999;

    $pages = paginate_links( array(
        'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
        'format' => '?paged=%#%',
        'prev_text' => 'Previous',
        'next_text' => 'Next',
        'current' => max( 1, get_query_var('paged') ),
        'total' => $wp_query->max_num_pages,
        'type'  => 'array',
    ) );

    return $pages;
}

add_action( 'wp_ajax_load_posts', 'load_posts' );
add_action( 'wp_ajax_nopriv_load_posts', 'load_posts' );
function load_posts() {

    /* sleep for testing purpose */
    /*sleep(2);*/

    $args = array(
        'posts_per_page'    =>  7,
        'post_type'         =>  array('news', 'post'),
        'paged'             =>  $_POST['page']
    );

    $cat_query = new WP_Query( $args );
    $GLOBALS['wp_query'] = $cat_query;

    if( ! $cat_query->have_posts() ) {
        get_template_part( 'template-parts/news-item-template', 'none' );
    }
    else {
        echo '<ul class="blogList clearfix">';
        while ( $cat_query->have_posts() ) {
            $cat_query->the_post();
            get_template_part( 'template-parts/news-item-template', get_post_format() );
        }
        echo '</ul>';
    }

    $pages = getPaginateLinks();
    set_query_var( 'pages', $pages );
    get_template_part('template-parts/pagination-template');

    wp_die();
}

add_action( 'wp_ajax_newsletter', 'newsletter' );
add_action( 'wp_ajax_nopriv_newsletter', 'newsletter' );
function newsletter() {

    if(isset($_POST['mail'])) :
        $mail = $_POST['mail'];
        global $wpdb;
        $table_name = "wordpress_newsletter_emails";

        $wpdb->insert(
            $table_name,
            array(
                'email' => $mail
            )
        );
        echo 'You are successfully subscribed to duck news. Thank you.';
    endif;

    wp_die(); // this is required to terminate immediately and return a proper response
}


?>