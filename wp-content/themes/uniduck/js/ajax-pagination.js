jQuery(document).ready(function($) {

	var $blogListContainer = $('#bloglistContainer');
	$(document).on('click', '.paginationItem', function (e) {
		e.preventDefault();
        $blogListContainer.addClass('loadingPosts').prepend('<span class="loadingText text-center">Loading posts. Please wait...</span>');
        $("html, body").animate({ scrollTop: 0 }, "slow");
        var data = {
            'action': 'load_posts',
			'page': getPageNumber($(this))
        };
        $.post(ajax_object.ajax_url, data, function(response) {

            $blogListContainer.empty();
            $blogListContainer.append(response);

        }).always(function() {
                $blogListContainer.removeClass('loadingPosts');
            });
    });

	function getPageNumber($elem){
		var number = 0;
		var $currentPage = $('.paginationItem.current');
		if($elem.hasClass('next')){
			number = $currentPage.parent().next().find('a').text();
		}else if($elem.hasClass('prev')){
			number = $currentPage.parent().prev().find('a').text();
		}else{
			number = $elem.text();
		}
		return number;
	}

});