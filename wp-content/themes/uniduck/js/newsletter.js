jQuery(document).ready(function($) {
    
    $('.newsletterForm').validate({
        submitHandler: function (form) {

            $submitButton = $(form).find('button[type="submit"]');
            $submitButton.addClass('clicked');

            var data = {
                'action': 'newsletter',
                'mail': $('#email').val()
            };

            jQuery.post(ajax_object.ajax_url, data, function(response) {
                $('.newsletterForm').slideUp();
                $('.responseMessage').text(response).slideDown(200);
            }).always(function () {
                clearTimeout(clickedTimeout);
                var clickedTimeout = setTimeout(function () {
                    $submitButton.removeClass('clicked');
                }, 300)
            });
        }
    })

});