<?php get_header(); ?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <article class="mainArticle">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-push-3">
                    <div class="articleInner">
                        <h1 class="title"><?php the_title(); ?></h1>
                        <?php the_content(); ?>
                        <?php get_template_part('template-parts/category-list-template'); ?>
                    </div>
                </div>
            </div>
        </div>
    </article>
<?php endwhile; endif; ?>
<?php get_footer(); ?>