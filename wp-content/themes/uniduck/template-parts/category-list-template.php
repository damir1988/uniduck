<?php
    $postType = get_post_type();
    $postTypeSlug = get_post_type_object($postType)->rewrite['slug'] ? get_post_type_object($postType)->rewrite['slug'] . '/' : '';
    $postCategories = wp_get_post_terms(get_the_ID(), get_post_taxonomies());
?>
<ul class="categoryList clearfix">
    <?php foreach ($postCategories as $category) : ?>
        <?php
        $categoryName = $category->name;
        $categorySlug = $category->slug;
        $categoryParentSlug = get_term( $category->parent, $category->taxonomy)->slug != null ? get_term( $category->parent, $category->taxonomy)->slug . '/' : '';
        ?>
        <li><a href="<?php echo get_site_url() .'/' . $postTypeSlug . $categoryParentSlug . $categorySlug ?>" class="categoryItem anim02"><?php echo $category->name; ?></a></li>
    <?php endforeach; ?>
</ul>