<?php $relatedPosts = related_post(); ?>
<?php if($relatedPosts->have_posts()) : ?>
<section class="moreMagicWrapper">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-lg-push-3 col-md-8 col-md-push-2">
                <h3 class="title f_medium c_pink text-center">More magic</h3>
                <ul class="moreArticleList">
                <?php  while($relatedPosts->have_posts()): $relatedPosts->the_post(); ?>
                        <li>
                            <div class="moreArticleItem <?php echo !has_post_thumbnail() ? 'moreArticleItemNoImg' : ''; ?>">
                                <?php if(has_post_thumbnail()) : ?>
                                    <img src="<?php echo get_the_post_thumbnail_url(get_the_ID(), "news_thumb_second"); ?>" srcset="<?php echo get_the_post_thumbnail_url(get_the_ID(), "news_thumb_second_retina"); ?> 2x" alt="">
                                <?php endif; ?>
                                <h4 class="title anim02"><?php the_title(); ?></h4>
                                <p><?php echo get_the_excerpt(); ?></p>
                            </div>
                        </li>
                <?php endwhile; ?>
                </ul>
            </div>
        </div>
    </div>
</section>
<?php endif; ?>