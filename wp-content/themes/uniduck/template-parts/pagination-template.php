<?php
if( is_array( $pages ) ) {
    $paged = ( get_query_var('paged') == 0 ) ? 1 : get_query_var('paged');
    echo '<div class="paginationWrapper"><ul class="paginationList">';
    foreach ( $pages as $page ) {
        $page = str_replace('page-numbers', 'paginationItem anim02', $page);
        if(strpos($page, 'prev') || strpos($page, 'next')){
            echo "<li class='prevNext'>$page</li>";
        }else{
            echo "<li>$page</li>";
        }
    }
    echo '</ul></div>';
}

?>
