<section class="newsletterWrapper">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center">
                <h3 class="title f_thin">Subscribe to duck news</h3>
                <form action="" class="newsletterForm form-inline">
                    <div class="form-group">
                        <input type="email" name="email" id="email" class="form-control" placeholder="Enter your e-mail address" required>
                    </div>
                    <button class="button buttonPrimary t_upper round anim02" type="submit">Subscribe</button>
                </form>
                <div class="responseMessage"></div>
            </div>
        </div>
    </div>
</section>