<li class="<?php echo !has_post_thumbnail() ? 'blogItemNoImage' : ''; ?>">
    <div class="blogItem">
        <?php if(has_post_thumbnail()) : ?>
            <img src="<?php echo get_the_post_thumbnail_url(get_the_ID(), $i > 0 ? "news_thumb" : "news_thumb"); ?>" srcset="<?php echo get_the_post_thumbnail_url(get_the_ID(), $i > 0 ? "news_thumb_second_retina" : "news_thumb_retina"); ?> 2x" alt="">
        <?php endif; ?>

        <span class="date"><?php echo get_the_date(); ?></span>
        <h2 class="title anim02"><?php the_title(); ?></h2>
        <?php get_template_part('template-parts/category-list-template'); ?>
        <p><?php echo get_the_excerpt(); ?></p>
        <ul class="commentsList clearfix">
            <li><span class="commentsItem anim02 icon-like">17 faves</span></li>
            <li><span class="commentsItem anim02 icon-comment"><?php comments_number(); ?></span></li>
        </ul>
    </div>
</li>