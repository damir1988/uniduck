<section class="searchBlogWrapper">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center">
                <?php if(is_page()) : ?>
                    <h2 class="title f_thin c_pink"><?php bloginfo('name'); ?></h2>
                <?php else : ?>
                    <h1 class="title f_thin c_pink"><?php bloginfo('name'); ?></h1>
                <?php endif; ?>
                <form class="searchForm" role="search" action="<?php echo site_url('/'); ?>" method="get">
                    <div class="form-group">
                        <input type="text" name="s" id="search" class="form-control icon-search text-center anim02" placeholder="Search blog" autocomplete="off" value="<?php echo get_query_var('s') ? get_query_var('s') : ''; ?>">
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>