<?php get_template_part('template-parts/search-template'); ?>
<?php ?>
<?php if (have_posts()) : ?>
<section class="blogListWrapper">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 anim02" id="bloglistContainer">
                <ul class="blogList clearfix">
                    <?php $postIndex = 0; ?>
                    <?php while ( have_posts() ) : the_post(); ?>
                        <?php get_template_part('template-parts/news-item-template'); ?>
                        <?php
                        $i++;
                    endwhile;
                    ?>
                </ul>
                <?php uniduck_pagination(); ?>
            </div>
        </div>
    </div>
</section>
<?php endif; ?>
<?php if(!have_posts() && (is_home() || is_archive())) : ?>
    <section class="noResultsWrapper">
        <div class="container">
            <div class="row">
                <div class="col-sm-8 col-sm-push-2 text-center">
                    <h4>Nothing found.</h4>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>
<?php if(!have_posts() && is_search()) : ?>
    <section class="noResultsWrapper">
        <div class="container">
            <div class="row">
                <div class="col-sm-8 col-sm-push-2 text-center">
                    <h4>Nothing found. Please change search parameters and try again.</h4>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>
<?php wp_reset_query(); ?>
